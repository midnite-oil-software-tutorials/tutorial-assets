Download openssl from https://www.openssl.org/

Command to create RSA key (replace mykey.key with your key name)
openssl genrsa -out mykey.key 2048

Command to create certificate signing erquest (.csr) 
openssl req -newkey rsa:2048 -keyout mykey.key -out mykey.csr

Command to create .pem file (prerequisite for .p12 file)
If necessary replace distribution.cer with the name of your .cer file
openssl x509 -in distribution.cer -inform DER -out mykey.pem -outform PEM

Command to create .p12 file
openssl pkcs12 -export -inkey mykey.key -in mykey.pem -out mykey.p12